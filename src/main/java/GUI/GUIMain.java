package GUI;

import Parsers.AnalizzatoreEmail;
import Parsers.MailParser;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.PrintStream;

public class GUIMain {
    private JPanel panelMain;
    private JTextArea textArea1;
    private JButton buttonFile;
    private JButton analizzaTestoButton;

    public GUIMain(){
        // operazioni preliminari
        JFrame frame = new JFrame("Email Check");
        frame.setContentPane(panelMain);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        panelMain.setSize(2000,600);

        // operazioni di popolazione
        inizializzaBottoni();

        // impacchettamento finale
        frame.pack();
        frame.setVisible(true);
    }

    private void inizializzaBottoni() {
        buttonFile.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                JFileChooser chooser = new JFileChooser();
                chooser.showOpenDialog(null);
                try {
                    MailParser mailParser = new MailParser(new FileInputStream(chooser.getSelectedFile()));
                    AnalizzatoreEmail analizzatoreEmail = new AnalizzatoreEmail(mailParser);
                    new OutputForm(analizzatoreEmail);
                } catch (IOException e1) {
                    e1.printStackTrace();
                }
            }
        });

        analizzaTestoButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                File out = new File("src/main/resources/inputFile.txt");
                try {
                    PrintStream ps = new PrintStream(out);
                    ps.print(textArea1.getText());
                    ps.close();
                    AnalizzatoreEmail analizzatoreEmail = new AnalizzatoreEmail(out.getPath());
                    new OutputForm(analizzatoreEmail);
                } catch (IOException e1) {
                    e1.printStackTrace();
                }
            }
        });
    }
}
