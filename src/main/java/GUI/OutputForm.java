package GUI;

import Parsers.AnalizzatoreEmail;

import javax.swing.*;

public class OutputForm {
    private AnalizzatoreEmail analizzatoreEmail;
    private JPanel panelOutput;
    private JLabel numeroParole;
    private JLabel numeroErrori;
    private JLabel percentualeErrori;
    private JLabel numeroBlacklist;
    private JLabel sogliaBlacklist;
    private JLabel labelUrlTesto;
    private JLabel labelUrlOggetto;
    private JLabel labelUrlMittente;
    private JLabel urlTesto;
    private JLabel labelAttendibilita;

    public static void main(String[] args) {
        new OutputForm();
    }

    public OutputForm(){
        // operazioni preliminari
        JFrame frame = new JFrame("Email Check");
        frame.setContentPane(panelOutput);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        panelOutput.setSize(2000, 600);

        frame.pack();
        frame.setVisible(true);
    }

    public OutputForm(AnalizzatoreEmail analizzatoreEmail) {
        // operazioni preliminari
        this.analizzatoreEmail = analizzatoreEmail;
        JFrame frame = new JFrame("Email Check");
        frame.setContentPane(panelOutput);
//        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
//        panelOutput.setSize(2000, 600);

        this.popolavista();

        frame.pack();
        frame.setVisible(true);
    }

    private void popolavista() {
        numeroParole.setText(analizzatoreEmail.getNumeroParole()+"");
        numeroErrori.setText(analizzatoreEmail.getNumeroErrori()+"");

        String percentualeformat = String.format("%.2f",analizzatoreEmail.getPercentualeErrori() );
        percentualeErrori.setText(percentualeformat+"%");

        numeroBlacklist.setText(analizzatoreEmail.getNumeroParoleBlacklist()+"");
        sogliaBlacklist.setText(analizzatoreEmail.getSogliaBlacklist());

        String urls = analizzatoreEmail.getUrls();
        if (urls==null) {
            urlTesto.setText("////");
        }else {
            urlTesto.setText(analizzatoreEmail.getUrls());
        }

        labelUrlTesto.setText(analizzatoreEmail.getConcordanzaUrlTesto());
        labelUrlMittente.setText(analizzatoreEmail.getConcordanzaUrlMittente());
        labelUrlOggetto.setText(analizzatoreEmail.getConcordanzaUrlOggetto());

        labelAttendibilita.setText(analizzatoreEmail.getAttendibilitaFromSorgente());
    }
}
