import Parsers.LanguageToolParser;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;

import java.io.*;
import java.util.HashMap;


import javax.swing.JFileChooser;

public class TrovaStringhe {

	public static void main(String[] args) throws IOException {
		HashMap<String, Integer> mappa = new HashMap<>();
		JFileChooser chooser = new JFileChooser();
		chooser.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);
		chooser.showOpenDialog(null);

		LanguageToolParser languageToolParser;
		
		for(File f : chooser.getSelectedFile().listFiles()) {
			if (f.isDirectory()) continue;
			languageToolParser = new LanguageToolParser(f.getPath());

			for (String lemma: languageToolParser.getLemmi()){
				if (mappa.containsKey(lemma)) {
					Integer occorrenza = mappa.get(lemma);
					mappa.put(lemma, occorrenza + 1);
				} else {
					mappa.put(lemma, 1);
				}
			}
		}
		stampaExcel(mappa);
		//print(mappa);
	}
	
	public static void print(HashMap<String, Integer> mappa) {
		for(String s : mappa.keySet()) {
			System.out.println(s+": "+mappa.get(s));
		}
	}

	public static void stampaExcel(HashMap<String, Integer> mappa) throws IOException {
		File resultFile = new File("ResultsFile_ParoleScam.xls");
		resultFile.createNewFile();

		Workbook workbook = new HSSFWorkbook();
		Sheet risultatiAnalisi = workbook.createSheet("RisultatiAnalisi");

		// intestazione
		Row row = risultatiAnalisi.createRow(0);
		row.createCell(0).setCellValue("Parole");
		row.createCell(1).setCellValue("Occorrenze");

		// corpo
		int indice = 1;
		for(String key: mappa.keySet()){
			row = risultatiAnalisi.createRow(indice);
			row.createCell(0).setCellValue(key);
			row.createCell(1).setCellValue(mappa.get(key));
			indice++;
		}

		FileOutputStream fileOut = new FileOutputStream(resultFile);
		workbook.write(fileOut);
		fileOut.close();

		// Chiude il workbook
		workbook.close();
		System.out.println("File di Risultati ResultsFile_ParoleScam.xls generato");
	}

}
