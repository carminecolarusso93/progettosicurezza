package Parsers;

import java.util.ArrayList;
import java.util.Scanner;

public class ParserErrori {
    private ArrayList<String> testo;
    private int numErrori;

    public ParserErrori(Scanner s) {
        testo= new ArrayList<>();
        this.numErrori=0;
        while (s.hasNext()){
            testo.add(s.nextLine());
        }


        calcolaErrori();
//        System.out.println(numErrori);
    }

    private void calcolaErrori() {
        //if(testo.size()>5){
            for(int i=0; i<testo.size();){

                String suggestion = testo.get(i+2);
                if (suggestion.startsWith("Suggestion")){
                    i = i + 6;
                }else{
                    i = i + 5;
                }

                numErrori++;
            }
      //  }
    }

    public int getNumErrori() {
        return numErrori;
    }
}
