package Parsers;

import javax.swing.*;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Scanner;

public class ParserURL {

    private ArrayList<String> testo;
    private ArrayList<String> urls;
    private ArrayList<String> dominiSecondoLivello;
    private String[] token;
    private MailParser mp;


    public ParserURL(MailParser mp){
        this.testo= new ArrayList<>();
        this.urls = new ArrayList<>();
        this.dominiSecondoLivello= new ArrayList<>();
        this.mp = mp;

        Scanner s = new Scanner(mp.getBody());


        while (s.hasNext()){
            testo.add(s.nextLine());
        }
        calcolaURL();


    }

    public ParserURL(Scanner s) throws IOException {
        this.testo= new ArrayList<>();
        this.urls = new ArrayList<>();
        this.dominiSecondoLivello= new ArrayList<>();
        this.mp = null;

        while (s.hasNext()){
            testo.add(s.nextLine());
        }
        calcolaURL();


    }

    private void calcolaURL(){
        for(int i=0; i<testo.size();i++){
            token = testo.get(i).split(" ");
            for(int j=0; j<token.length; j++){
                try{
                    URL url= new URL(token[j]);
                    urls.add(token[j]);
                    String autority = url.getAuthority();
                    String[] domini = autority.split("\\.");
                    dominiSecondoLivello.add(domini[domini.length-2]);
                }catch (MalformedURLException e){
                    continue;
                }
            }
        }
    }

    public boolean concordanzaUrlTesto() {
        for(int i=0; i<dominiSecondoLivello.size();i++) {
            for (int j = 0; j < testo.size(); j++) {
                token= testo.get(j).split(" ");

                for(int k=0; k<token.length; k++) {

                    String parola = token[k].toLowerCase();

                    if (parola.length() == 0)
                        continue;
                    if (dominiSecondoLivello.get(i).toLowerCase().contains(parola)) {

                        if (dominiSecondoLivello.get(i).length() > parola.length()){

                            String dominioRestante = dominiSecondoLivello.get(i).substring(token[k].length());
                            System.out.println("dominioRestante = " + dominioRestante);

                            if (k<token.length-1 && dominioRestante.toLowerCase().contains(token[k+1].toLowerCase())){

                                System.out.println("Concordanza: " + dominiSecondoLivello.get(i).toUpperCase() + " con il testo1. " + token[k]);
                                return true;
                            }

                        }
                        else if (dominiSecondoLivello.get(i).length() == token[k].length()){
                            System.out.println("Concordanza: " + dominiSecondoLivello.get(i).toUpperCase() + " con il testo2. "+ token[k]);
                            return true;
                        }
                    }
                }
            }
        }
        System.out.println("Non c'è concordanza con il testo e l'url.");
        return false;

    }

    public boolean concordanzaUrlOggetto(){
        if (mp != null) {
            String subject = mp.getSubject();
            System.out.println("subject = " + subject);
            String[] token = subject.split(" ");
            for(int k = 0; k<token.length; k++) {
                for (int i = 0; i < dominiSecondoLivello.size(); i++) {

                    String parola = token[k].toLowerCase();

                    if (parola.length() == 0)
                        continue;
                    if (dominiSecondoLivello.get(i).toLowerCase().contains(parola)) {

                        if (dominiSecondoLivello.get(i).length() > parola.length()){

                            String dominioRestante = dominiSecondoLivello.get(i).substring(token[k].length());
                            System.out.println("dominioRestante = " + dominioRestante);

                            if (k<token.length-1 && dominioRestante.toLowerCase().contains(token[k+1].toLowerCase())){

                                System.out.println("Concordanza: " + dominiSecondoLivello.get(i).toUpperCase() + " con l'Oggetto. " + token[k]);
                                return true;
                            }

                        }
                        else if (dominiSecondoLivello.get(i).length() == token[k].length()){
                            System.out.println("Concordanza: " + dominiSecondoLivello.get(i).toUpperCase() + " con l'Oggetto. "+ token[k]);
                            return true;
                        }
                    }

//                    if (dominiSecondoLivello.get(i).toLowerCase().contains(token[j].toLowerCase())) {
//                        System.out.println("Il dominio " + dominiSecondoLivello.get(i).toUpperCase() + " corrisponde all'oggetto: " + subject.toUpperCase());
//                        return true;
//                    }
                }
            }
        }
        System.out.println("Non è presente il dominio di secondo livello nell'oggetto!");
        return false;
    }

    public boolean concordanzaUrlMittente() {
        if (mp != null) {
            String mittente = mp.getFrom();
//            System.out.println("mittente = " + mittente);
//            System.out.println("mittente = " + mittente);
//            System.out.println("mittente.split(\"@\",2)[1] = " + mittente.split("@",2)[1]);
            String[] dominioMittenteSplit = mittente.split("@",2)[1].split("\\.");
            String dominioMittente = dominioMittenteSplit[dominioMittenteSplit.length-2];


            System.out.println("dominioMittente = " + dominioMittente);

            for (int i = 0; i < dominiSecondoLivello.size(); i++) {

                if (dominiSecondoLivello.get(i).toLowerCase().equals(dominioMittente.toLowerCase())) {
                    System.out.println("Il dominio " + dominiSecondoLivello.get(i).toUpperCase() + " corrisponde al mittente: " + mittente.toUpperCase());
                    return true;
                }
            }
        }

        System.out.println("Non è presente il dominio di secondo livello nel mittente!");
        return false;
    }

    public boolean presenzaURL(){
        return !urls.isEmpty();
    }

    public ArrayList<String> getUrls(){
        return urls;
    }
}
