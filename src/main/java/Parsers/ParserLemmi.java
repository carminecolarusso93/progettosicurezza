package Parsers;

import java.util.ArrayList;
import java.util.Scanner;

public class ParserLemmi {

    private ArrayList<String> testo;
    private ArrayList<String> lemmi;
    private int conteggioParole;

    public ParserLemmi(Scanner sErr) {
        conteggioParole=0;
        testo = new ArrayList<>();
        lemmi = null;
        while (sErr.hasNext()){
            String s = sErr.nextLine();
//            System.out.println("s = " + s);
            testo.add(s);
        }
        calcolaLemmi();

        /*for(String s: lemmi){
            System.out.println("s = " + s);
        }*/

    }

    public ArrayList<String> getLemmi(){
        if (lemmi == null){
            this.calcolaLemmi();
        }
        return lemmi;
    }

    private void calcolaLemmi() {
        //ogni tre righe c'è l interpretazione di LanguageTools
        lemmi = new ArrayList<>();
        for (int i = 3; i<testo.size(); i=i+3){

//            System.out.println("*********** "+i+" ********");
            String[] token = testo.get(i).split(" ");
            for (int j = 1; j<token.length; j++){

//                System.out.println(j + " " + token[j]);
                String lemma = controllaLemma(token[j]);
                if (lemma != null){
                    if (!lemma.contains("[")) {
                        lemmi.add(lemma);
                    }
                }
                conteggioParole++;
            }
        }
    }

    private String controllaLemma(String s) {
        int indice1 = s.indexOf("[");
        int indice2 = s.indexOf("]");
//        System.out.println("s = " + s);

        try {
            String analisiLanguageTool = s.substring(indice1+1, indice2);
//            System.out.println("analisiLanguageTool = " + analisiLanguageTool);
            String[] sceltaMultipla =  analisiLanguageTool.split(",");
            String[] tmp;
//            if(sceltaMultipla.length<2) {
            return verificaStopword(sceltaMultipla[0]); //todo
//            return verificaStopword(sceltaMultipla[sceltaMultipla.length-1]);
//            }else{
//                System.err.println("AAAAA");
//                return null;
//            }
//
        }catch (StringIndexOutOfBoundsException e){
            return null;
        }
//        return null;
    }

    private String verificaStopword(String s) {
//        System.out.println("stopword = " + s);
        String[] tmp;
        tmp = s.split("/");
//        int indice = tmp.length-1;
        int indice = tmp.length-1;
        if (tmp[indice].contains("PRE") //preposizioni
                ||tmp[indice].contains("DET")
                ||tmp[indice].contains("PRO") //pronomi
                ||tmp[indice].contains("CON") //congiunzioni
                ||tmp[indice].contains("ART") //articoli
                ||tmp[0].contains("<")
                ||tmp[indice].contains("null")){
            return null;
        }else{
            return tmp[0];
        }
    }

    public int getNumeroParole(){
        return conteggioParole;
    }
}
