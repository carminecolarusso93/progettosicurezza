package Parsers;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.PrintStream;
import java.util.ArrayList;
import java.util.Scanner;

public class AnalizzatoreEmail {
    private ParserBlacklist parserBlacklist;
    private LanguageToolParser languageToolParser;
    private ParserURL parserURL;
    private int contatoreParoleBlacklist;
    private MailParser mailParser;


    // costruttore con mail da file
    public AnalizzatoreEmail(MailParser mailParser) throws IOException {
        this.mailParser=mailParser;
        this.parserBlacklist = new ParserBlacklist("src/main/resources/Blacklist.txt");
        File out = new File("src/main/resources/inputFile.txt");
        PrintStream ps = new PrintStream(out);
        ps.print(mailParser.getBody());
        ps.close();
        languageToolParser = new LanguageToolParser(out.getPath());
        parserURL = new ParserURL(mailParser);

        calcolaNumeroParoleBlacklist();

    }



// costruttore solo testo
    public AnalizzatoreEmail(String path) throws IOException {
        this.mailParser = null;
        this.parserBlacklist = new ParserBlacklist("src/main/resources/Blacklist.txt");
        this.languageToolParser =  new LanguageToolParser(path);
        this.parserURL = new ParserURL(new Scanner(new File(path)));

        System.out.println("*********** Errori *************");


        System.out.println("languageToolParser.getNumErrori() = " + languageToolParser.getNumErrori());
        System.out.println("languageToolParser.getPercentualeErrori() = " + languageToolParser.getPercentualeErrori());

//        System.out.println("*********** Lemmi *************");
//        for(String s : languageToolParser.getLemmi()){
//            System.out.println("lemma = " + s + "\t\tparserBlacklist =" +parserBlacklist.isDangerous(s));
//        }

        System.out.println("*********** Url *************");
        System.out.println("parserURL = " + parserURL.presenzaURL());

        for (String s : parserURL.getUrls()){
            System.out.println("url = " + s);
        }

        calcolaNumeroParoleBlacklist();

    }

    public int getNumeroParole() {
        return languageToolParser.getNumeroParole();
    }

    public int getNumeroErrori() {
        return languageToolParser.getNumErrori();
    }

    public double getPercentualeErrori() {
        return languageToolParser.getPercentualeErrori();
    }

    public int getNumeroParoleBlacklist(){
        return contatoreParoleBlacklist;
    }

    private void calcolaNumeroParoleBlacklist(){
        this.contatoreParoleBlacklist = 0;
        System.out.println("*********** Lemmi *************");
        for(String s : languageToolParser.getLemmi()) {
            System.out.println("lemma = " + s);
            if (parserBlacklist.isDangerous(s)) {
                System.out.println("blacklist = " + s);
                contatoreParoleBlacklist++;
            }
        }
    }

    public String getSogliaBlacklist() {
        double percentuale = getNumeroParoleBlacklist() / getNumeroParole() *100;

        percentuale = 100-percentuale;
        if (percentuale<20){
            return "Inaffidabile";
        }else if(20<=percentuale && percentuale<40){
            return "Scarsa";
        }else if(40<=percentuale && percentuale<60){
            return "Affidabile";
        }else if(60<=percentuale && percentuale<80){
            return "Buona";
        }else{
            return "Ottima";
        }
    }

    public String getUrls() {
        ArrayList<String> urls = parserURL.getUrls();
        if(urls.isEmpty())
            return null;
        StringBuilder builder = new StringBuilder("<html>");
        for (String s : parserURL.getUrls()){
            builder.append(s);
            builder.append(" <br> ");
        }
        builder.append("</html>");
        return builder.toString();
    }


    public String getConcordanzaUrlTesto(){
        if (parserURL.concordanzaUrlTesto())
            return "Si";
        return "No";
    }

    public String getConcordanzaUrlMittente(){
        if (parserURL.concordanzaUrlMittente())
            return "Si";
        return "No";
    }

    public String getConcordanzaUrlOggetto(){
        if (parserURL.concordanzaUrlOggetto())
            return "Si";
        return "No";
    }

    public String getAttendibilitaFromSorgente(){
        if (mailParser == null)
            return "////";

        String from = mailParser.getFrom();
        int ind1 = from.indexOf("[");
        int ind2 = from.lastIndexOf("]");
        System.out.println("mailParser.sorgente = " + mailParser.getSorgente());
        String fromRipulito = from.substring(ind1+1,ind2);
        System.out.println("fromRipulito = " + fromRipulito);

        if (mailParser.getSorgente().equals(fromRipulito)){
            return "Si";
        }else
            return "No";
    }
}
