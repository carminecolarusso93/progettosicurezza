package Parsers;


import java.io.File;
import java.io.FileNotFoundException;
import java.util.HashSet;
import java.util.Scanner;

public class ParserBlacklist {
    private HashSet<String> blacklist;

    public ParserBlacklist(String blaclistPath) throws FileNotFoundException {
        this.blacklist = new HashSet<>();
        File blacklistFile = new File(blaclistPath);
        Scanner scannerBlacklist = new Scanner(blacklistFile);

        while (scannerBlacklist.hasNext()){
            blacklist.add(scannerBlacklist.nextLine());
        }
    }

    public boolean isDangerous(String string){
        return blacklist.contains(string);
    }
}
