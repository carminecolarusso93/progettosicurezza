package Parsers;

import com.google.common.io.CharStreams;

import java.io.*;

import org.apache.james.mime4j.dom.Entity;
import org.apache.james.mime4j.dom.Multipart;
import org.apache.james.mime4j.dom.TextBody;
import org.apache.james.mime4j.dom.Message;
import org.apache.james.mime4j.message.DefaultMessageBuilder;


public class MailParser {
    private StringBuilder textBuilder;
    private StringBuilder htmlBuilder;
    private Message m;

    public MailParser(FileInputStream fileInputStream) throws IOException {
        this.m = new DefaultMessageBuilder().parseMessage(fileInputStream);
        this.textBuilder = new StringBuilder();
        this.htmlBuilder = new StringBuilder();
//        System.out.println("m.getHeader().getField(\"Return-Path\") = " + m.getHeader().getField("Return-Path"));
        this.parse(m);
    }

    public String getFrom(){
        return m.getFrom().toString();
    }

    public String getTo(){
        return m.getTo().toString();
    }

    public String getSubject(){
        return m.getSubject();
    }

    public String getBody(){
        return textBuilder.toString();
    }

    public String getBodyHtml(){
        return htmlBuilder.toString();
    }

    public String getSorgente(){
        String returnPath = m.getHeader().getField("Return-Path").toString();


        if(returnPath.contains("<")) {
            int ind1 = returnPath.indexOf("<");
            int ind2 = returnPath.lastIndexOf(">");
            return returnPath.substring(ind1+1, ind2);
        }else{
//            String r = returnPath.substring(returnPath.indexOf("Return-Path: "));
            String r = returnPath.split(" ",2)[1];
//            System.err.println("a "+r);
            return r;
        }

    }

    public void parse(Entity part) throws IOException {
        if (isPlainOrHtml(part.getMimeType()) && !isAttachment(part.getDispositionType())) {
            TextBody tb = (TextBody) part.getBody();
            String result = CharStreams.toString(new InputStreamReader(tb.getInputStream(), tb.getMimeCharset()));
            if (part.getMimeType().equals("text/plain")) {
                textBuilder.append(result);
            } else if (part.getMimeType().equals("text/html")) {
                htmlBuilder.append(result);
            }
        } else if (isMultipart(part.getMimeType())) {
            Multipart multipart = (Multipart) part.getBody();
            for (Entity e : multipart.getBodyParts()) {
                parse(e);
            }
        }
    }

    private static boolean isPlainOrHtml(String mimeType) {
        return (mimeType.equals("text/plain") || mimeType.equals("text/html"));
    }

    private static boolean isMultipart(String mimeType) {
        return mimeType.startsWith("multipart/");
    }

    private static boolean isAttachment(String dispositionType) {
        return dispositionType != null && dispositionType.equals("attachment");
    }


}