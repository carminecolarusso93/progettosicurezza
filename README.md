progettoSicurezza


Guida all'installazione

1. scaricare il programma TreeTagger

2. scaricare la lingua desiderata

3. estrarre dagli archivi i file scaricati

4. nella cartella in cui sono stat estratti i file aprire il terminale e lanciare il comando
    sh install-tagger.sh

5. nella cartella lib rinominare il nome della lingua nel seguente modo
    es. italian.par -> italian-utf8.par

6. installare la libreria wrapper per python mediante il seguente comando
    pip3 install treetaggerwrapper

